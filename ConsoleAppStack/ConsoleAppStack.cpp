﻿#include <iostream>
using namespace std;

class Stack
{
public:

    Stack(){setlocale(LC_ALL, "ru");FillArray(DinamArray, size);}

    ~Stack(){delete[] DinamArray;}

    void FillArray(int* const arr, const int size)
    {
        for (int i = 0; i < size; i++)
        {
            arr[i] = rand() % 10;
        }
    }


    void ShowArray(const int* const arr, const int size)
    {
        for (int i = 0; i < size; i++)
        {
            cout << arr[i] << "\t";
        }
        cout << endl;
    }

    void push(const int value)
    {
        cout << "Old  = ";
        ShowArray(DinamArray, size);

        int* newArray = new int[size + 1];

        for (int i = 0; i < size; i++)
        {
            newArray[i] = DinamArray[i];
        }

        newArray[size] = value;
        size++;
        delete[] DinamArray;
        DinamArray = newArray;

        cout<< "Push = ";
        ShowArray(DinamArray, size);
    }

    void pop()
    {
        int* newArray = new int[--size];

        for (int i = 0; i < size; i++)
        {
            newArray[i] = DinamArray[i];
        }
        delete[] DinamArray;
        DinamArray = newArray;

        cout << "Pop  = ";
        ShowArray(DinamArray, size);
    }

    void back()
    {
        int b = size - 1;
        cout << "Последний элемент = " << DinamArray[b] << endl;
        b = NULL;
    }

    void Size(){cout << "Кол-во элементов в массиве = " << size << endl;}

private:
    int size = 1;
    int* DinamArray = new int[size];
};

int main()
{
    Stack s1;
    s1.push(99);
    s1.push(190);
    s1.push(777);
    s1.pop();
    s1.pop();
    s1.push(5);
    s1.back();
    s1.push(89);
    s1.back();
    s1.push(12);
    s1.back();
    s1.Size();
    s1.push(69);
    s1.back();
    s1.Size();
    s1.push(34);
    s1.Size();
}